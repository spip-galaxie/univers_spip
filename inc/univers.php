<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

include_spip('inc/filtres');
include_spip('inc/distant');
include_spip('inc/meta');

/**
 * Proposer un site en l'enregistrant en base.
 *
 * @param      string   $url    URL du site propose
 *
 * @return     integer  0 (false) ou l'id en base du site propose
 */
function univers_proposer_site($url) {
	$parts = parse_url($url);

	if (!$parts) {
		return 0;
	}

	// pas d'IP fixe !
	if (preg_match(';^[0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}[.][0-9]{1,3}$;', $parts['host'])) {
		return 0;
	}
	// pas de fausse url venant de feedburner ou google !
	// on pourrait faire mieux en suivant la redirection pour
	// attraper le vrai nom du site
	if (preg_match(',(feeds\.feedburner\.com|feedproxy\.google\.com),i', $parts['host'])) {
		return 0;
	}

	$base = $parts['scheme'] . '://' . $parts['host'] . ($parts['path'] ?? '');
	$base = univers_nettoyer_url($base);
	$base = (string) preg_replace(',(spip[.]php3?).*$,i', '\\1', $base);
	$url_clean = univers_url_clean($base);

	if (
		$id_website = sql_getfetsel('id_website', 'spip_websites', 'url=' . sql_quote($base)) or
		$id_website = sql_getfetsel(
			'id_website',
			'spip_websites',
			'url_clean=' . sql_quote($url_clean),
			'',
			'date DESC',
			'0,1'
		)
	) {
		return $id_website;
	}

	$id_website = sql_insertq(
		'spip_websites',
		[
			'url' => $base,
			'url_clean' => $url_clean,
			'descriptif' => '',
			'date' => date('Y-m-d H:i:s')
		]
	);

	// relire et verifier
	$url = sql_getfetsel('url', 'spip_websites', 'id_website=' . intval($id_website));
	if ($url !== $base) {
		// il y a eu un caractere exotique dans l'url, on la vire
		sql_delete('spip_websites', 'id_website=' . intval($id_website));
		return 0;
	}

	return $id_website;
}

/**
 * Reparer les urls mal formees.
 *
 * @param      string  $url    URL a nettoyer
 *
 * @return     string  URL nettoyee
 */
function univers_nettoyer_url($url) {
	$url = (string) preg_replace(',(IMG|local)/cache.+spip[.]php,', 'spip.php', $url);
	$url = (string) preg_replace(',(index|spip)[.]php/.+,i', 'spip.php', $url);
	$url = (string) preg_replace(',(index|spip|forum|article)[.]php3/.+,i', '', $url);
	$url = (string) preg_replace(',/(plugins|ecrire|IMG|local|squelettes)/.+[.]php,i', '', $url);

	return $url;
}

/**
 * Calculer une url nettoyée du protocole, /, spip.php
 * qui permet de mieux gérer les doublons
 *
 * @param string $url
 *
 * @return string
 */
function univers_url_clean($url) {
	$url = (string) preg_replace('#^https?://#i', '', $url);
	$url = (string) preg_replace('#^www[.]#i', '', $url);
	$url = (string) preg_replace('#spip[.]php$#i', '', $url);
	$url = rtrim($url, '/');

	return $url;
}

/**
 * Nettoyage en base des URLs enregistrees malformees.
 *
 * @return void
 */
function univers_nettoyer_urls() {
	// urls mal formees
	$res = sql_select(
		'id_website,url',
		'spip_websites',
		"url REGEXP '(index|spip)[.]php/.+'"
					. " OR url REGEXP '/(plugins|ecrire|IMG|local|squelettes)/.+[.]php$'"
	);
	while ($row = sql_fetch($res)) {
		$url = univers_nettoyer_url($row['url']);
		if ($url != $row['url']) {
			spip_log('nettoyage ' . $row['id_website'] . ':' . $row['url'] . " => $url", 'universclean');
			sql_updateq('spip_websites', ['url' => $url], 'id_website=' . intval($row['id_website']));
		}
	}

	// urls refusees (feed google/feedburner)
	sql_delete('spip_websites', 'url LIKE' . sql_quote('http://feeds.feedburner.com%'));
	sql_delete('spip_websites', 'url LIKE' . sql_quote('http://feedproxy.google.com%'));

	// nettoyage des liens morts (suite a delete ci-dessus)
	$res = sql_select(
		'P.id_website',
		'spip_websites_plugins AS P LEFT JOIN spip_websites AS W ON P.id_website = W.id_website',
		'W.id_website IS NULL'
	);
	while ($row = sql_fetch($res)) {
		sql_delete('spip_websites_plugins', 'id_website=' . intval($row['id_website']));
	}
}

/**
 * Tache periodique d'analyse des referers de contrib depuis une date donnee.
 *
 * @param      string  $date   Date au format Y-m-d (SQL)
 *
 * @return void
 */
function univers_rechercher_referers($date) {
	$where_base = [
		'date=' . sql_quote($date),
		'referer NOT LIKE ' . sql_quote('http%://%.spip.net%'),
		'referer NOT LIKE ' . sql_quote('http%://%.spip.org%'),
		'referer NOT LIKE ' . sql_quote('http%://%spip-contrib.net%'),
		'referer NOT LIKE ' . sql_quote('localhost'),
	];

	$where = $where_base;
	$where[] = 'referer LIKE ' . sql_quote('%spip.php%');
	$res = sql_select('referer', 'spip_referers', $where);
	spip_log("Import depuis les referer du $date : %spip.php% , " . sql_count($res), 'universreferers');
	while ($row = sql_fetch($res)) {
		spip_log('Import referer : ' . $row['referer'], 'universreferers');
		univers_proposer_site($row['referer']);
	}

	$where = $where_base;
	$where[] = 'referer LIKE ' . sql_quote('%/ecrire/%');
	$res = sql_select('referer', 'spip_referers', $where);
	spip_log("Import depuis les referer du $date : %/ecrire/% , " . sql_count($res), 'universreferers');
	while ($row = sql_fetch($res)) {
		spip_log('Import referer : ' . $row['referer'], 'universreferers');
		univers_proposer_site(preg_replace(',/ecrire/.*$,Uims', '/spip.php', $row['referer']));
	}
}

/**
 * @param string $req
 * @param integer $start
 * @param integer $max
 * @param integer $step
 * @param string $var
 *
 * @return array<string, string>
 */
function univers_rechercher_sites_spip($req, $start = 0, $max = 10, $step = 10, $var = 'start') {
	$urls = [];
	while ($start < $max) {
		$url = parametre_url($req, $var, $start, '&');
		$page = recuperer_url($url);
		$page = $page['page'] ?? '';
		$h3 = extraire_balises($page, 'h3');
		foreach ($h3 as $h) {
			$a = extraire_balise($h, 'a');
			$href = extraire_attribut($a, 'href');
			if (
				preg_match(';^([a-z]{3,5})://;i', $href)
				and strpos($href, 'inurl:') === false
				and strpos($href, 'google') === false
				and strpos($href, 'spip.php') !== false
			) {
				$href = preg_replace(',spip[.]php?.*$,i', 'spip.php', $href);
				$urls[$href] = textebrut($a);
			}
		}
		$start += $step;
	}
	return $urls;
}

/**
 * Recuperer les URLs des sites a verifier.
 *
 * @param      string   $url      Adresse du flux RSS
 *
 * @return     string[]    Liste des sites a verifier
 */
function univers_twitter_extraire_feed_urls($url) {
	$long = [];
	$urls = [];
	$page = recuperer_url($url);
	$page = $page['page'] ?? '';
	$page = str_replace('&lt;b&gt;', '', $page);
	$page = str_replace('&lt;/b&gt;', '', $page);

	$titles = extraire_balises($page, 'title');
	$page = (string) preg_replace(',</?title>,ims', "\n", implode('', $titles));

	preg_match_all(",https?://[^?\"'#;:\s]*,ims", $page, $regs, PREG_SET_ORDER);
	$urls = array_map('reset', $regs);
	foreach ($urls as $k => $url) {
		if (!preg_match(",https?://[^?\"'#;:]*spip[.]php3?,Uims", (string) $url)) {
			// essayer de l'elargir
			if (!isset($long[$url])) {
				$long[$url] = recuperer_url($url, ['taille_max' => 100000]);
				$long[$url] = (is_string($long[$url]['url']) ? $long[$url]['url'] : false);
			}
			if ($long[$url]) {
				$urls[$k] = $url = $long[$url];
			}
		}
		if (!preg_match(",https?://[^?\"'#;:]*spip[.]php3?,Uims", $url)) {
			unset($urls[$k]);
		}
	}
	$urls = array_unique($urls);

	return $urls;
}

/**
 * Recuperer les URLs des sites a verifier.
 *
 * @param      string  $url    Adresse du flux RSS
 *
 * @return     string[]   Liste des sites a verifier
 */
function univers_spipnet_extraire_feed_urls($url) {
	$urls = [];
	$page = recuperer_url($url);
	$page = $page['page'] ?? '';
	$links = extraire_balises($page, 'link');
	foreach ($links as $link) {
		if (preg_match(',<link>(.*)</link>,Uims', $link, $reg)) {
			$urls[] = trim($reg[1]);
		}
	}
	$urls = array_unique($urls);

	return $urls;
}

/**
 * Vérifie un coup sur 2 les flux RSS du blog et du site officiel.
 *
 * @return void
 */
function univers_feed_watch() {
	$explore = [
		[
			'twitter',
			'https://blog.spip.net/?page=backend-twitter-spip'
		],
		[
			'spipnet',
			'https://www.spip.net/?page=backend-sites-sous-spip&id_article=884'
		],
	];
	$feed = $GLOBALS['meta']['univers_feedwatch'] ?? 0;
	list($type, $url) = $explore[$feed];

	//Verification d'un flux
	if (function_exists($f = "univers_$type" . '_extraire_feed_urls')) {
		spip_log($s = "Analyse Feed $url", 'univers');
		$liste = $f($url);

		foreach ($liste as $url) {
			spip_log($s = "$url", 'univers');
			univers_proposer_site($url);
		}
	}

	// Alternance des flux a verifier
	$feed++;
	if ($feed >= count($explore)) {
		$feed = 0;
	}
	ecrire_meta('univers_feedwatch', $feed);

	// un coup de netoyage sur les urls mal formees
	univers_nettoyer_urls();
}
