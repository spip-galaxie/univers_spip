<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

// User agent used to load the page
@define(
	'_INC_DISTANT_USER_AGENT',
	'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.6; fr; rv:1.9.1.9) Gecko/20100315 Firefox/3.5.9'
);
@define('_INC_DISTANT_VERSION_HTTP', 'HTTP/1.0');

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('inc/filtres');
include_spip('inc/distant');
include_spip('inc/meta');

/**
 * Get the complete page.
 * Should be sub.something.tld/spip.php
 *
 * Some site set a cookie then redirect before everything else
 * so we have to accept it
 *
 * @param string $url
 * @param string $cookie
 *
 * @return string[]
 *
 */
function univers_recuperer_lapage($url, $cookie = '', $max = 10) {
	$ref = $GLOBALS['meta']['adresse_site'];
	// let's say we're coming from google, after all...
	$GLOBALS['meta']['adresse_site'] = 'http://www.google.fr';
	$datas = ''
	 . "Cookie: $cookie\r\n"
	 . "\r\n"
	;

	$site = recuperer_url($url, ['datas' => $datas]);
	$GLOBALS['meta']['adresse_site'] = $ref;

	if (!$site) {
		return [];
	}
	$status = $site['status'] ?? 0;
	$header = $site['headers'] ?? '';
	$page = $site['page'] ?? '';
	// si on a fait toutes les redirections possibles (ie: pas de redirection infinie)
	// utiliser l’url finale
	if (empty($site['location'])) {
		$url = $site['url'] ?? $url;
	}

	// if a cookie set, accept it an retry with it
	if (--$max >= 0) {
		if (preg_match(',Set-Cookie: (.*)(;.*)?$,Uims', $header, $r)) {
			//ne pas relancer si le cookie est déjà présent
			if (strpos($cookie, $r[1]) === false) {
				$cookie .= $r[1] . ';';
				spip_log("Cookie : $cookie on repart pour un tour ", 'univers_check');
				return univers_recuperer_lapage($url, $cookie, $max);
			}
		}
	} else {
		spip_log("Problème de cookie sur le site $url", 'univers_check');
	}

	return [$status, $header, $page, $url];
}

/**
 * Get address by host.
 * Use nslookup instead of php function
 *
 * @param string $host
 * @param int $timeout
 * @return string
 */
function univers_getaddrbyhost($host, $timeout = 3) {
	$ip = gethostbyname($host);
	if ($ip and $ip !== $host) {
		return $ip;
	}

	if (is_callable('shell_exec') && false === stripos((string) ini_get('disable_functions'), 'shell_exec')) {
		$query = `nslookup -timeout=$timeout -retry=1 $host`;
		if (preg_match('/\nAddress: (.*)\n/', $query, $matches)) {
			return trim($matches[1]);
		}
	}

	return $host;
}

/**
 * Analyse a site to check it's made with SPIP and find versions and plugins
 *
 * @param string $url
 * @param bool $debug
 *
 * @return array<string, mixed> un tableau de donnees
 */
function univers_analyser($url, $debug = false) {
	$res = [];

	spip_log("analyse version : $url ", 'univers_check');
	$path = parse_url($url);
	if (!$path) {
		return [];
	}

	$ip = univers_getaddrbyhost($path['host']);
	if (!$ip or $ip == $path['host']) {
		return [];  // pas d'ip, pas de site, rien a faire
	}

	$res['ip'] = $ip;

	
	$url_source = $url;
	// ne pas conserver de spip.php dans l’url
	if (strpos($url, '/spip.php') !== false) {
		$url = rtrim((string) preg_replace('/spip\.php$/', '', $url), '/') . '/';
	}
	// get the complete page
	$site = univers_recuperer_lapage($url);
	if (empty($site)) {
		$res['response'] = false;

		return $res;
	}

	list($status, $header, $page, $redirect_url) = $site;
	$status = intval($status);
	if ($debug) {
		spip_log(var_export([$status, $header, $redirect_url], true), 'univers_check' . _LOG_DEBUG);
	}

	if ($url_source !== $redirect_url) {
		$res['response'] = '301';
		$res['location'] = $redirect_url;

		return $res;
	}

	if (404 === $status) {
		$res['response'] = '404';

		return $res;
	}

	// get some generic informations (server, php, gzip, length)
	if (preg_match(',Server: (.*)$,mi', $header, $r)) {
		$res['server'] = $r[1];
	}
	if (preg_match(',X-Powered-By: PHP/(.+)$,mi', $header, $r)) {
		$res['php'] = $r[1];
	}
	if (preg_match(',Content-Encoding: gzip$,mi', $header)) {
		$res['gzip'] = true;
	}
	if (preg_match(',Content-Length: ([0-9]+)$,mi', $header, $r)) {
		$res['length'] = $r[1];
	}

	// check if the header says "Hey, i'm made with SPIP"
	$regexp_plugin_spip = ',Composed-By: (?:.*)\+ (?<plugins>spip\((?<spip>[^)]+)\)(.*)?)$,mi';
	$regexp_generique = ',Composed-By: (?<spip>.*)( @ www.spip.net)( ?\+ ?(?<plugins>.*))?$,mi';
	if (
		preg_match($regexp_plugin_spip, $header, $r)
		or preg_match($regexp_generique, $header, $r)
	) {
		spip_log('visite de "' . $url . '". Résultat: ' . var_export($r, true), 'univers_check' . _LOG_DEBUG);
		if ($r[1] === 'SPIP') {
			// en cas de header silencieux, le fichier local/config.txt renvoie une page 404 inutile à récupérer
			$res['spip'] = '?';
			$res['plugins'] = [];
			spip_log('visite de "' . $url . '". Résultat: header silencieux', 'univers_check' . _LOG_DEBUG);
		} else {
			$config = null;
			// local/config.txt est directement indiqué
			if (strpos($r['plugins'] ?? '', 'local/config.txt') !== false) {
				$url_config = $r['plugins'];
				$config = univers_recuperer_lapage($url_config);
				spip_log(
					'visite de "' . $url_config . ' indiqué". Résultat: HTTP Status ' . var_export($config[0] ?? null, true),
					'univers_check' . _LOG_DEBUG
				);
			} 
			// ou le tenter local/config.txt sinon, car plus complet si le header semble coupe
			elseif (substr($header, -1) !== ')') {
				$url_config = suivre_lien($url, 'local/config.txt');
				$config = univers_recuperer_lapage($url_config);
				spip_log(
					'visite de "' . $url_config . ' exploré". Résultat: HTTP Status ' . var_export($config[0] ?? null, true),
					'univers_check' . _LOG_DEBUG
				);
				if (200 !== intval($config[0] ?? null)) {
					$url_mutu = str_replace('ww2.', '', str_replace('www.', '', $path['host']));
					$url_config = suivre_lien($url, "sites/$url_mutu/local/config.txt");
					$config = univers_recuperer_lapage($url_config);
					spip_log(
						'visite de "' . $url_config . ' exploré". Résultat: HTTP Status ' . var_export($config[0] ?? null, true),
						'univers_check' . _LOG_DEBUG
					);
				}
			}
			// On teste le status http une seconde fois au cas où on est tombé sur une mutu
			if (200 === intval($config[0] ?? null)) {
				spip_log('Visite de "' . $url_config . '". Résultat:' . $config[2], 'univers_check' . _LOG_DEBUG);
				if (
					preg_match($regexp_plugin_spip, $config[2], $rc)
					or preg_match($regexp_generique, $config[2], $rc)
				) {
					$r = $rc;
				}
			}
			$res['spip'] = trim(preg_replace(',^[^0-9]*,', '', $r['spip']));
			if (!$res['spip']) {
				$res['spip'] = '?';
			}
			$res['plugins'] = [];
			$plugins = array_values(array_filter(array_map('trim', explode(',', $r['plugins'] ?? ''))));
			if (
				count($plugins) === 1
				&& (strpos($plugins[0], 'local/config.txt') !== false)
			) {
				// il n’a pas été possible de lire le contenu du fichier indiqué
				$plugins = [];
			}
			foreach ($plugins as $plugin) {
				$res['plugins'][preg_replace(',[(].*,', '', $plugin)] = $plugin;
			}
		}
	}

	// get ecrire/paquet.xml ^^
	if (!isset($res['spip']) || $res['spip'] === '?') {
		$url_paquet = suivre_lien($url, 'ecrire/paquet.xml');
		$paquet = univers_recuperer_lapage($url_paquet);
		spip_log(
			'Visite de "' . $url_paquet . '". Status:' . $paquet[0] . '. headers:' . var_export($paquet[1], true),
			'univers_check' . _LOG_DEBUG
		);
		if (200 === intval($paquet[0]) && strpos($paquet[1], 'Content-Type: application/xml')) {
			$xml = $paquet[2];
			if (false !== strpos($xml, '<paquet')) {
				try {
					$simple_xml = new SimpleXMLElement($xml);
					$res['spip'] = '' . current($simple_xml->xpath('/paquet/@version'));
				} catch (Exception $e) {
					spip_log($e->getMessage(), 'univers_check' . _LOG_DEBUG);
				}
			}
		} else {
			spip_log(
				'Visite de "' . $url_paquet . '". Pas un spip ou plus vieux que SPIP3.0',
				'univers_check' . _LOG_INFO
			);
		}
	}

	// else, find another clue
	// if 'spip_' is in the html, there are some chance that it is a SPIP site
	if (!isset($res['spip'])) {
		if (preg_match(',spip_(in|out|logo),i', $page)) {
			$res['spip'] = '?';
		}
	}

	// if maybe but not sure, try to get the login page
	// it should have some information that says "SPIP"
	if (isset($res['spip']) and $res['spip'] === '?') {
		// recuperer la page de login
		$login = preg_replace(',spip[.]php.*$,', '', $url) . 'ecrire/';
		if ($login = univers_recuperer_lapage($login)) {
			$login = $login[2] ?? '';
			if (
				preg_match(',<meta name=[\'"]generator["\'][^>]*>,Uims', $login, $r)
				and $v = extraire_attribut($r[0], 'content')
				and preg_match(',SPIP ([^\[]+),', $v, $r)
			) {
				$res['spip'] = trim($r[1]);
			}
		}
	}

	// if we did'nt found login page, or there whas no information
	// try to get the htaccess.txt delivered with SPIP,
	// it has some extra informations
	if (isset($res['spip']) and $res['spip'] === '?') {
		// tenter de recup le htaccess.txt qui contient un numero de version
		$ht = preg_replace(',spip[.]php.*$,', '', $url) . 'htaccess.txt';
		if ($ht = univers_recuperer_lapage($ht)) {
			$htpage = $ht[2] ?? '';
			if (preg_match(',SPIP v\s*([0-9.]+),', $htpage, $r)) {
				$res['spip'] = $r[1];
			}
		}

		// if we didn't found a confirmation and there was only 'spip' in the html
		// maybe it's an old spip site, but it’s no more a SPIP now.
		if ($res['spip'] === '?') {
			unset($res['spip']);
		}
	}

	// if it is a 404, that was a bad adress
	if (count($res) == 1 and preg_match(',404 ,', $header)) {
		$res['response'] = '404';
	} else {
		// else record the page size and so on
		$res['size'] = strlen($page);
		$res['response'] = true;
		if (
			false !== stripos($page, 'Fatal error')
			and false !== stripos($page, 'Call to undefined function')
		) {
			$res['response'] = 'fatal';
		}

		if (preg_match(',<title>(.*)</title>,Uims', $page, $r)) {
			$res['title'] = $r[1];
		}
		if (preg_match(',<meta name=[\'"]description["\'][^>]*>,Uims', $page, $r)) {
			$res['description'] = extraire_attribut($r[0], 'content');
		}
	}

	return $res;
}

/**
 * Take one record to check in database, and process it.
 *
 * @param      array<string, mixed>    $row    The row
 * @param      bool  $debug  The debug mode for the exec web page
 *
 * @return bool|string
 */
function univers_analyser_un($row, $debug = false) {
	if ($debug) {
		var_dump($row);
	}
	$id = $row['id_website'];
	$url = $row['url'];

	// incrementer le retry et la date avant tout pour ne pas tourner en rond
	// sur ce site si il timeout
	sql_updateq(
		'spip_websites',
		['retry' => $row['retry'] + 1, 'date' => date('Y-m-d H:i:s'), 'status' => 'timeout?'],
		'id_website=' . intval($id)
	);

	$res = univers_analyser($url, $debug);
	if ($debug) {
		var_dump($res);
	}

	$set = [];
	if ($res === []) {
		$set['retry'] = ++$row['retry'];
		$set['status'] = 'no-dns';
	} elseif (
		$res['response'] === false
		or $res['response'] === '404'
		or $res['response'] === 'fatal'
	) {
		$set['ip'] = $res['ip'];
		$set['retry'] = ++$row['retry'];
		$set['status'] = ($res['response'] ? $res['response'] : 'dead');
	} elseif ($res['response'] === '301') {
		spip_log("Site $id $url — Nouvelle URL : " . $res['location'], 'univers_check' . _LOG_DEBUG);
		$set['url'] = $res['location'];
		$set['retry'] = 0;
		$set['statut'] = 'prop';
		$set['status'] = '';
		include_spip('inc/univers');
		$set['url_clean'] = univers_url_clean($res['location']);
	} elseif ($res['response'] === true) {
		$set['ip'] = $res['ip'];
		$set['server'] = !empty($res['server']) ? $res['server'] : '';
		$set['php'] = !empty($res['php']) ? $res['php'] : '';
		$set['gzip'] = !empty($res['gzip']) ? 'oui' : '';
		$set['length'] = !empty($res['length']) ? $res['length'] : 0;
		$set['size'] = !empty($res['size']) ? $res['size'] : 0;

		if (isset($res['spip'])) {
			$set['pays'] = univers_geoip($set['ip']);

			// c'estun SPIP !
			$set['spip'] = $res['spip'];
			$set['statut'] = 'publie';
			if (!empty($res['title'])) {
				$set['titre'] = $res['title'];
			}
			if (!empty($res['description'])) {
				$set['descriptif'] = $res['description'];
			}

			// mettre a jour les plugins
			sql_delete('spip_websites_plugins', 'id_website=' . intval($id));
			if (isset($res['plugins']) && is_array($res['plugins'])) {
				$set['plugins'] = count($res['plugins']);
				foreach ($res['plugins'] as $p => $v) {
					sql_insertq('spip_websites_plugins', ['id_website' => $id,'plugin' => $p,'version' => $v]);
				}
			} else {
				$set['plugins'] = 0;
			}

			if ($debug) {
				var_dump(univers_geoip($set['ip']));
			}
		} else {
			// c'en est pas un !
			$set['statut'] = 'refuse';
		}
		$set['status'] = '';
		$set['retry'] = 0;
	} else {
		// ???
		$set['retry'] = ++$row['retry'];
		$set['status'] = $res['response'];
	}

	$set['date'] = date('Y-m-d H:i:s');
	if ($debug) {
		var_dump($set);
	}
	return sql_updateq('spip_websites', $set, 'id_website=' . intval($id));
}

/**
 * Find state information from IP adress with GeoIP tool
 *
 * @staticvar string $gi
 * @param string $ip
 * @return string
 */
function univers_geoip($ip = null) {
	static $gi = null;
	if (is_null($ip)) {
		include_spip('geoip/geoip');
		geoip_close($gi);
		return '';
	}
	if (is_null($gi)) {
		include_spip('geoip/geoip');
		$gi = geoip_open(find_in_path('geoip/GeoIP.dat'), GEOIP_STANDARD);
	}

	return (string) geoip_country_code_by_addr($gi, $ip);
}


/**
 * Remonte d’url d’un dossier, si possible
 * 
 * https://truc.muche/dir1/dir2/spip.php
 * => https://truc.muche/dir1/
 * => https://truc.muche/
 * => null
 */
function univers_url_parent(string $url): ?string {
	$desc = parse_url($url);
	if (!$desc) {
		return null;
	}

	$actual_path = $desc['path'];
	$path = str_replace('/spip.php', '', $actual_path);
	$path = dirname($path);
	$path = rtrim($path, '/') . '/';
	$new_url = str_replace($actual_path, $path, $url);
	if ($new_url !== $url) {
		return $new_url;
	}
	return null;
}
