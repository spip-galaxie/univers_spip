<?php

/**
 * Statisitiques en JSON sans passer par un squelette html.
 */
function action_api_stats_dist(): void
{
	$arg = _request('arg');
	$arg = explode('/', $arg);

	if (!in_array($arg[0], ['spip', 'plugin'], true)) {
		send(['error' => 'spip or plugin as first argument']);
	}
	if ($arg[0] == 'spip') {
		$version = isset($arg[1]) && is_valid_version($arg[1]) ? $arg[1]  : '';
		$response = sql_select_spip($version);
	} else {
		$prefix  = isset($arg[1]) && is_valid_prefix($arg[1]) ? $arg[1] : '';
		$version = isset($arg[2]) && is_valid_version($arg[2]) ? $arg[2] : '';
		$response = sql_select_plugin($prefix, $version);
	}

	send($response);
}

/**
 * Récupère en base les sites actifs.
 *
 * @param string $version Pour réduire la demande à une version SPIP donnée
 *
 * @return array<string, mixed>
 */
function sql_select_spip(string $version): array
{
	$where = ['statut=\'publie\''];
	$groupby = ['`version`'];
	if (preg_match(',^\d+\.\d+(.\d+)?$,', $version)) {
		// famille x.y(.z)
		$where[] = 'spip LIKE \'' . $version . '%\'';
	}

	$spip = [];
	$total = 0;
	$php = [];

	$res = sql_select([
		'REGEXP_REPLACE(spip, \'^([0-9]+\.[0-9]+' . ($version ? '\.[0-9]+' : '') . ').*$\', \'\\\\1\') AS `version`',
		'COUNT(spip) AS sites'
	], ['spip_websites'], $where, $groupby);
	while ($site = sql_fetch($res)) {
		$spip[] = ['version' => $site['version'], 'sites' => intval($site['sites'])];
		$total += $site['sites'];
	}
	$res = sql_select([
		'REGEXP_REPLACE(php, \'^([0-9]+\.[0-9]+).*$\', \'\\\\1\') AS `version`',
		'COUNT(php) AS sites'
	], ['spip_websites'], $where, $groupby);
	while ($site = sql_fetch($res)) {
		if ($site['version']) {
			$php[] = ['version' => $site['version'], 'sites' => intval($site['sites'])];
		}
	}

	$response = ['total_sites' => $total, 'versions' => $spip, 'php' => $php];
	if ($version) {
		$response['version'] = $version;
	}

	return  $response;
}

/**
 * Récupère en base les plugins de sites actifs.
 *
 * @param string $prefix  Pour réduire la demande à un plugin donné
 * @param string $version Pour réduiie la demande à une version d'un plugin donné
 * @return array<string, mixed>
 */
function sql_select_plugin(string $prefix, string $version): array
{
	$plugins = [];
	$where = '';

	if ($prefix) {
		$where .= ' AND plugins.plugin = \'' . $prefix . '\'';
	}
	if ($version) {
		$where .= ' AND REGEXP_REPLACE(plugins.version, \'.+\\\\(([0-9.]+)\\\\)\',  \'\\\\1\') LIKE \'' . $version . '%\'';
	}

	$res = sql_query('SELECT
	plugins.plugin AS prefix,
	REGEXP_REPLACE(plugins.version, \'.+\\\\(([0-9.]+)\\\\)\',  \'\\\\1\') AS `version`,
	COUNT(plugins.plugin) AS sites
FROM spip_websites AS sites
LEFT JOIN spip_websites_plugins AS plugins
ON sites.id_website = plugins.id_website
WHERE sites.statut = \'publie\'' . $where . '
GROUP BY prefix, `version`
ORDER BY prefix, `version`');
	while ($site = sql_fetch($res)) {
		if ($site['prefix']) {
			$plugins[] = ['prefix' => $site['prefix'], 'version' => $site['version'], 'sites' => intval($site['sites'])];;
		}
	}

	return ['plugins' => $plugins];
}

/**
 * Valide une chaine de caractère comme étant une version mineure ou un patch.
 */
function is_valid_version(string $version): bool
{
	return (bool) preg_match(',^\d+\.\d+(\.\d+)?$,', $version);
}

/**
 * Valide une chaine de caractères comme étant un préfixe de plugin.
 */
function is_valid_prefix(string $prefix): bool
{
	return (bool) preg_match(',^\w+$,', $prefix);
}

/**
 * Envvoie une reponse http en json.
 *
 * @param array<string, mixed> $response
 * @return void
 */
function send(array $response): void
{
	header('Content-Type: application/json');
	header('Access-Control-Allow-Origin: *');
	echo json_encode($response, JSON_THROW_ON_ERROR);
	exit(0);
}
