<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

function univers_upgrade(string $nom_meta_base_version, string $version_cible): void {
	$maj = [];

	$maj['create'] = [
		['maj_tables', ['spip_websites', 'spip_websites_plugins']],
		['ecrire_config', 'univers_feedwatch', 1]
	];

	include_spip('base/upgrade');
	maj_plugin($nom_meta_base_version, $version_cible, $maj);
}

function univers_vider_tables(string $nom_meta_base_version): void {
	sql_drop_table('spip_websites');
	sql_drop_table('spip_websites_plugins');

	effacer_meta('univers_feedwatch');
	effacer_meta($nom_meta_base_version);
}
