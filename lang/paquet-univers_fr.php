<?php

// Ceci est un fichier langue de SPIP -- This is a SPIP language file

// Fichier produit par PlugOnet
// Module: paquet-univers
// Langue: fr
// Date: 25-09-2019 17:59:23
// Items: 2

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// U
	'univers_description' => 'Une photo temps reel de l\'univers des sites SPIP',
	'univers_slogan' => 'Une photo temps reel de l\'univers des sites SPIP',
);
?>