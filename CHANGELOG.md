# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html) and
[Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/).

## [Unreleased]

### Added

- `/stats.api/spip` ou `/stats.api/spip/3.2` ou `/stats.api/spip/3.2.19`
- `/stats.api/plugin` ou `/stats.api/plugin/breves` ou `/stats.api/plugin/breves/1.4` ou `/stats.api/plugin/breves/1.4.2`
