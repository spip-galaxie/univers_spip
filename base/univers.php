<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * @param array<string, mixed> $interface
 * @return array<string, mixed>
 */
function univers_declarer_tables_interfaces($interface) {
	$interface['table_des_tables']['websites'] = 'websites';

	return $interface;
}

/**
 * @param array<string, mixed> $tables
 *
 * @return array<string, mixed>
 */
function univers_declarer_tables_principales($tables) {
	$tables['spip_websites'] = [
		'type' => 'website',
		'principale' => 'oui',
		'field' => [
			'id_website' => 'bigint(21) NOT NULL',
			'url' => "varchar(255) default '' NOT NULL",
			'titre' => "text DEFAULT '' NOT NULL",
			'descriptif' => "text DEFAULT '' NOT NULL",
			'ip' => "varchar(255) default '' NOT NULL",
			'spip' => "varchar(255) default '' NOT NULL",
			'server' => "varchar(255) default '' NOT NULL",
			'php' => "varchar(255) default '' NOT NULL",
			'gzip' => "varchar(3) default '' NOT NULL",
			'length' => 'bigint(21) NOT NULL',
			'size' => 'bigint(21) NOT NULL',
			'plugins' => 'bigint(21) default NULL',
			'pays' => "char(3) default '' NOT NULL",
			'url_clean' => "varchar(255) default '' NOT NULL",

			'date' => "datetime DEFAULT '0000-00-00 00:00:00' NOT NULL",
			'statut' => "varchar(10) default 'prop' NOT NULL",

			'retry' => 'int(5) default 0 NOT NULL',
			'status' => "varchar(10) default '' NOT NULL",
		],
		'key' => [
			'PRIMARY KEY' => 'id_website',
		],
	];

	return $tables;
}


/**
 * @param array<string, mixed> $tables_auxiliaires
 *
 * @return array<string, mixed>
 */
function univers_declarer_tables_auxiliaires($tables_auxiliaires) {
	$tables_auxiliaires['spip_websites_plugins'] = [
		'field' => [
			'id_website' => 'bigint(21) NOT NULL',
			'plugin' => "varchar(64) default '' NOT NULL",
			'version' => "varchar(255) default '' NOT NULL",

		],
		'key' => [
			'PRIMARY KEY' => 'id_website, plugin',
		],
	];

	return $tables_auxiliaires;
}
