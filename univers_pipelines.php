<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

/**
 * Taches periodiques de verification des sites en SPIP.
 *
 * @param array<string, int> $taches_generales Les taches generales
 *
 * @return array<string, int> Les nouvelles taches generales
 */
function univers_taches_generales_cron(array $taches_generales) {
	$taches_generales['univers_feed'] = 6 * 3600;
	$taches_generales['univers_referers'] = 12 * 3600;
	$taches_generales['univers_check'] = 97;
	$taches_generales['univers_check_doublons'] = 12 * 3600;
	$taches_generales['univers_refuse2prop'] = 24 * 3600;

	return $taches_generales;
}

/**
 * Optimiser la base de données en supprimant les liens de sites orphelins
 *
 * @pipeline optimiser_base_disparus
 *
 * @param array<string, mixed> $flux Données du pipeline
 * @return array<string, mixed> Données du pipeline
 */
function univers_optimiser_base_disparus($flux) {
	$mydate = sql_quote($flux['args']['date']);

	sql_delete('spip_websites', "statut='poub' AND `date` < $mydate LIMIT 10000");
	sql_delete('spip_websites_plugins', 'id_website NOT IN (select id_website from spip_websites)');

	return $flux;
}
