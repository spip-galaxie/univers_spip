<?php

function find_in_path($fileName)
{
    return $fileName;
}

function balise_ENV_dist($p, $src = '')
{
    $p->code = '(is_array($a = (' . $src . ')) ? serialize($a) : "")';

    return $p;
}

function include_spip($fileName)
{

}

function _request($arg): string
{
	return '';
}

function recuperer_fond($fond): string
{
	return '';
}

function spip_log($message = null, $name = null)
{

}

function ecrire_meta($nom, $valeur, $importable = null, $table = 'meta')
{
	return '';
}

function ecrire_fichier($fichier, $contenu, $ignorer_echec = false, $truncate = true)
{
	
}

function spip_timer($t = 'rien', $raw = false)
{

}

function effacer_meta($nom, $table = 'meta')
{
	
}

function parametre_url($url, $c, $v = null, $sep = '&amp;')
{
	
}

function suivre_lien($url, $lien)
{
	
}

function sql_query($sql)
{

}
