<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

/**
 * Tache periodique d'analyse de remise en proposition de site injustement refusés ou poubellisés
 *
 * @param      mixed   $t      Unused parameter
 * @return     integer  0
 */
function genie_univers_refuse2prop_dist($t) {
	sql_updateq(
		'spip_websites',
		['statut' => 'prop', 'status' => '', 'retry' => 0],
		'statut="refuse" AND `date` < DATE_SUB(CURRENT_DATE, INTERVAL 2 YEAR) ORDER BY `date` ASC LIMIT 100'
	);

	return 0;
}
