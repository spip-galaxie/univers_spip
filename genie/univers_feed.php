<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

/**
 * Tache periodique d'examen des flux RSS pour decouvrir et proposer des nouveaux sites SPIP.
 *
 * @param      mixed   $t      Unused parameter
 *
 * @return     integer  0
 */
function genie_univers_feed_dist($t) {
	include_spip('inc/univers');
	univers_feed_watch();

	return 0;
}
