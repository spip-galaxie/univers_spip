<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

/**
 * Tache periodique d'analyse des referers de contrib de moins d'un jour.
 *
 * @param      mixed   $t      Unused parameter
 *
 * @return     integer  0
 */
function genie_univers_referers_dist($t) {
	include_spip('inc/univers');
	univers_rechercher_referers(date('Y-m-d', time() - 24 * 3600));

	return 0;
}
