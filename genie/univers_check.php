<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

/**
 * Tache periodique d'analyse des sites decouverts.
 *
 * @param      mixed   $t      Unused parameter
 *
 * @return     integer  0
 */
function genie_univers_check_dist($t) {
	include_spip('inc/univers');
	include_spip('inc/univers_analyser');

	univers_check_nettoyages();
	univers_check_analyses();
	spip_log('Fin univers_check', 'univers_check');

	return 0;
}

function univers_check_analyses() {

	
	// 5 sites en attente de validation
	$il_y_a_une_heure = date('Y-m-d H:i:s', time() - 3600);
	$res = sql_allfetsel(
		'*',
		'spip_websites',
		"statut='prop' AND (retry=0 OR date<" . sql_quote($il_y_a_une_heure) . ')',
		'',
		'date,retry',
		'0,5'
	);
	if ($res) {
		$ids = array_column($res, 'id_website');
		spip_log(sprintf('Analyse %s sites en attente : %s', count($ids), implode(', ', $ids)), 'univers_check');
		foreach ($res as $row) {
			univers_analyser_un($row);
		}
	}

	// revisiter 5 sites deja vu, en commencant par les plus anciens
	$il_y_a_quatre_heure = date('Y-m-d H:i:s', time() - 4 * 3600);
	$res = sql_allfetsel(
		'*',
		'spip_websites',
		"statut='publie' AND (retry=0 OR date<" . sql_quote($il_y_a_quatre_heure) . ')',
		'',
		'date,retry',
		'0,5'
	);
	if ($res) {
		$ids = array_column($res, 'id_website');
		spip_log(sprintf('Analyse %s sites publiés : %s', count($ids), implode(', ', $ids)), 'univers_check');
		foreach ($res as $row) {
			univers_analyser_un($row);
		}
	}

	// revisiter un site publie, en retry de plus de 4 heures
	$res = sql_allfetsel(
		'*',
		'spip_websites',
		"statut='publie' AND (retry>0 AND date<" . sql_quote($il_y_a_quatre_heure) . ')',
		'',
		'date,retry',
		'0,1'
	);
	if ($res) {
		$ids = array_column($res, 'id_website');
		spip_log(sprintf('Analyse %s sites publiés en échec : %s', count($ids), implode(', ', $ids)), 'univers_check');
		foreach ($res as $row) {
			univers_analyser_un($row);
		}
	}

	// revisiter un site refusé depuis plus de 3 mois, si pas SPIP => poubelle
	$il_y_a_3_mois = date('Y-m-d H:i:s', strtotime('-3 month'));
	$res = sql_allfetsel(
		'*',
		'spip_websites',
		"statut='refuse' AND date < " . sql_quote($il_y_a_3_mois),
		'',
		'date',
		'0,1'
	);
	if ($res) {
		$ids = array_column($res, 'id_website');
		spip_log(sprintf('Analyse %s sites refusé : %s', count($ids), implode(', ', $ids)), 'univers_check');
		foreach ($res as $row) {
			univers_analyser_un($row);
			$statut = sql_getfetsel('statut', 'spip_websites', 'id_website = ' . $row['id_website']);
			if ($statut === 'refuse') {
				$id = $row['id_website'];
				$url = $row['url'];
				spip_log("Site $id $url — Suppression (Non SPIP)", 'univers_check' . _LOG_DEBUG);
				sql_delete('spip_websites_plugins', 'id_website = ' . $id);
				sql_updateq('spip_websites', ['statut' => 'poub'], 'id_website = ' . $id);
			}
		}
	}
}

function univers_check_nettoyages() {
	spip_log('Nettoyage des sites en erreur', 'univers_check');

	// passer a la poubelle les sites sans DNS et essayes au moins 5 fois
	sql_updateq(
		'spip_websites',
		['statut' => 'poub'],
		"statut IN ('prop','publie') AND status='no-dns' AND retry>=5"
	);

	// passer a la poubelle les sites morts et essayes au moins 10 fois
	// soit un propose pas vu vivant dans les 10 dernieres heures
	// soit un publie (donc vu vivant un jour) pas vu vivant dans les 40 dernieres heures
	sql_updateq(
		'spip_websites',
		['statut' => 'poub'],
		"statut IN ('prop','publie') AND status IN ('dead', 'fatal') AND retry>=10"
	);

	// revisiter un site 404, en retry, en cherchant son url parente
	$res = sql_allfetsel(
		'id_website, url',
		'spip_websites',
		"statut IN ('prop','publie') AND retry>=10 AND status='404'",
		'',
		'retry DESC, date',
		'0,10'
	);

	foreach ($res as $row) {
		if ($new_url = univers_url_parent($row['url'])) {
			sql_updateq(
				'spip_websites', 
				[
					'statut' => 'prop', 
					'url' => $new_url, 
					'retry' => 0, 
					'status' => '',
					'url_clean' => univers_url_clean($new_url),
				], 
				"id_website = " . $row['id_website']
			);
		} else {
			sql_updateq('spip_websites', ['statut' => 'poub'], "id_website = " . $row['id_website']);
		}
	}

	// synchroniser les url_clean 
	$res = sql_allfetsel(
		'id_website, url',
		'spip_websites',
		"url NOT LIKE concat('%', url_clean, '%')",
		'',
		'date',
		'0,10'
	);
	foreach ($res as $row) {
		sql_updateq(
			'spip_websites', 
			['url_clean' => univers_url_clean($row['url'])], 
			"id_website = " . $row['id_website']
		);
	}

}
