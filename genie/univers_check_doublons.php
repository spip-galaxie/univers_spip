<?php

/**
 * Plugin Univers SPIP
 * (c) 2010 Cedric
 * Distribue sous licence GPL
 */

/**
 * Tache periodique d'analyse de nettoyage des doublons
 *
 * @param      mixed   $t      Unused parameter
 * @return     integer  0
 */
function genie_univers_check_doublons_dist($t) {
	spip_timer('up');
	$websites = sql_allfetsel(
		['url_clean', 'count(*) as nb_doublons'],
		'spip_websites',
		"(statut='publie' AND status='') OR statut='refuse' OR statut = 'prop'",
		'url_clean',
		'',
		'0,50',
		'nb_doublons > 1'
	);
	$websites = array_column($websites, 'url_clean');
	foreach ($websites as $website) {
		$doublons = sql_allfetsel(
			'id_website',
			'spip_websites',
			['url_clean = ' . sql_quote($website)],
			'',
			["IF(LEFT(url, 5) = 'https', 1, 0) DESC", 'date DESC'],
			'1,100'
		);
		$doublons = array_column($doublons, 'id_website');
		sql_updateq(
			'spip_websites',
			['statut' => 'poub'],
			sql_in('id_website', $doublons)
		);
	}
	spip_log(count($websites) . ' sites doublons supprimés en ' . spip_timer('up'), 'univers_check');

	return 0;
}
